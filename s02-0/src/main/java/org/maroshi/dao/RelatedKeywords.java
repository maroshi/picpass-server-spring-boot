package org.maroshi.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import lombok.Data;

@Data
@Entity
public class RelatedKeywords  {
	@Id
	private Long   id;
	private String name;
	private int    count;

}
