package org.maroshi.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import lombok.Data;

@Data
@Entity
public class KeywordPicsCount {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   id;
	private Long   picsCount;
	private String name;
	private String category;
}
