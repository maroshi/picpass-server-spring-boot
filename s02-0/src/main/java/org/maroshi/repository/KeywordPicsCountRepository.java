package org.maroshi.repository;

import java.util.List;

import org.maroshi.dao.KeywordPicsCount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface KeywordPicsCountRepository extends CrudRepository<KeywordPicsCount, Long> {
	@Query(nativeQuery = true,
		       value = "SELECT K.ID AS id, count(K.ID) AS pics_count, K.NAME as name, K.CATEGORY as category FROM KEYWORD K JOIN PICTURE_KEYWORD_RT PK ON (k.ID = pk.KEYWORDS_ID) GROUP BY K.ID, K.NAME, K.CATEGORY ORDER BY 2 desc")
	List<KeywordPicsCount> all();
}
