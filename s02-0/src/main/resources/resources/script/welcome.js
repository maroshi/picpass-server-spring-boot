var _availableKeywordsArr = [ "ActionScript", "AppleScript", "C++", "Clojure" ];

$('#keyword-textbox').keyup(function(event) {
	if (event.keyCode === 13) {
		$('#search-button').click();
	}
});

$('#search-button').click(function(event) {
	searchKeyword($('#keyword-textbox').val());
});

var _loadedKeywordsArr = [];

function getUrlParameter(name) {
	let params = new window.URLSearchParams(window.location.search);
	return params.get(name);
}

function loadKeywordsArrFnc() {
	$.getJSON(`/api/keywordPicsCounts/search/all`, function(respData) {
		_availableKeywordsArr = respData._embedded.keywordPicsCounts;
		_availableKeywordsArr.forEach(function(element) {
			// console.log(element.name);
			_loadedKeywordsArr.push(element.name);
		});
		
		// search input parameter if provided
		let searchKeywordParam = getUrlParameter('k');
		if (searchKeywordParam != null) {
			$('#keyword-textbox').val(searchKeywordParam);
			searchKeyword(searchKeywordParam);
		}
	});
}

var _showKeyword = false;

function selectedKeywordFnc(event, ui) {
	if (ui.item == null)
		return;
	searchKeyword(ui.item.value);
}

function searchKeyword(keyword) {
	_showKeyword = false;
	let keywordIdx = _loadedKeywordsArr.indexOf(keyword);
	if (keywordIdx < 0) { // not found
		let message = `Not found any ${keyword} keyword.`;
		if (keyword.length == 0) message = '';
		$('#message-box-article').text(message);
		$('#message-box-article').toggle(true);
		$('#keyword-article').toggle(false);
		return;
	}
	_showKeyword = true;
	$('#message-box-article').toggle(false);
	$('#keyword-article').toggle(true);
	let keywordObj = _availableKeywordsArr.find((o) => { return o['name'] === keyword });
	$('#keyword-text').val(keywordObj.name);
	$('#keyword-category').val(keywordObj.category);
	$('#keyword-pics-count').val(keywordObj.picsCount);

	// load pictures
	let pictureObjsArr = [];
	let pictureImageSrcArr = [];
	$('#pictures-list').empty();
	let urlSegmentsArr = keywordObj._links.self.href.split('/');
	let keywordObjId = urlSegmentsArr[urlSegmentsArr.length - 1];
	$.getJSON(`/api/keywords/${keywordObjId}/pictures`, function(respData) {
		pictureObjsArr = respData._embedded.pictures;
		pictureObjsArr.forEach(function(element, idx) {
			let pictureLink = `/api/pictures/download/${element.id_}`;
			// console.log(element.name);
			pictureImageSrcArr.push(pictureLink);
			let li = $('<li/>').appendTo('#pictures-list');
			$('<img>', {
				id : `${element.name}`,
				src : `${pictureLink}`,
				title : `${element.name}\n${element.id_}`
			}).appendTo(li);
		});
		// register click event on images
		$('img').click(onPictureUpdate);
	});

	// load related keywords
	let relatedKeywordsObsArr = [];
	$('#related-keywords-list').empty();
	$.getJSON(
			`/api/relatedKeywordses/search/findRelatedByName?name=${keyword}`,
			function(respData) {
				relatedKeywordsObsArr = respData._embedded.relatedKeywordses;
				relatedKeywordsObsArr.forEach(function(element, idx) {
					let keywordLink = `/k1.html?k=${element.name}`;
					let li = $('<li/>').appendTo('#related-keywords-list');

					$('<a />').text(`${element.name} (${element.count})`).attr(
							'href', keywordLink).attr('target', '_blank')
							.appendTo(li);
				});
			});
}

$("#keyword-textbox").autocomplete({
	source : _loadedKeywordsArr,
	create : loadKeywordsArrFnc,
	delay : 250,
	select : selectedKeywordFnc
});

$('img').tooltip({
});

$('#message-box-article').toggle(true);
$('#keyword-article').toggle(false);