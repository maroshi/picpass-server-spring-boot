package org.maroshi.repository;

import java.util.List;

import org.maroshi.dao.RelatedKeywords;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface RelatedKeywordsRepository extends CrudRepository<RelatedKeywords, Long> {
	@Query(nativeQuery = true, 
			value = "SELECT kwdpct2.KEYWORDS_ID id, kwd2.NAME name, count(kwdpct2.KEYWORDS_ID) count FROM KEYWORD kwd1 JOIN PICTURE_KEYWORD_RT kwdpct1 ON kwd1.ID = kwdpct1.KEYWORDS_ID JOIN PICTURE_KEYWORD_RT kwdpct2 ON kwdpct1.PICTURES_ID = kwdpct2.PICTURES_ID AND kwdpct2.KEYWORDS_ID <> kwd1.id JOIN KEYWORD kwd2 ON kwdpct2.KEYWORDS_ID = kwd2.ID WHERE kwd1.ID = ?1 GROUP BY kwdpct2.KEYWORDS_ID, kwd2.name, kwd1.id ORDER BY count desc" )
	List<RelatedKeywords> findRelatedById(Long id);
	@Query(nativeQuery = true, 
			value = "SELECT kwdpct2.KEYWORDS_ID id, kwd2.NAME name, count(kwdpct2.KEYWORDS_ID) count FROM KEYWORD kwd1 JOIN PICTURE_KEYWORD_RT kwdpct1 ON kwd1.ID = kwdpct1.KEYWORDS_ID JOIN PICTURE_KEYWORD_RT kwdpct2 ON kwdpct1.PICTURES_ID = kwdpct2.PICTURES_ID AND kwdpct2.KEYWORDS_ID <> kwd1.id JOIN KEYWORD kwd2 ON kwdpct2.KEYWORDS_ID = kwd2.ID WHERE kwd1.name = ?1 GROUP BY kwdpct2.KEYWORDS_ID, kwd2.name, kwd1.id ORDER BY count desc" )
	List<RelatedKeywords> findRelatedByName(String name);
	@Query(nativeQuery = true, 
			value = "SELECT kwdpct2.KEYWORDS_ID id, kwd2.NAME name, count(kwdpct2.KEYWORDS_ID) count FROM KEYWORD kwd1 JOIN PICTURE_KEYWORD_RT kwdpct1 ON kwd1.ID = kwdpct1.KEYWORDS_ID JOIN PICTURE_KEYWORD_RT kwdpct2 ON kwdpct1.PICTURES_ID = kwdpct2.PICTURES_ID AND kwdpct2.KEYWORDS_ID <> kwd1.id JOIN KEYWORD kwd2 ON kwdpct2.KEYWORDS_ID = kwd2.ID GROUP BY kwdpct2.KEYWORDS_ID, kwd2.name, kwd1.id ORDER BY count desc" )
	List<RelatedKeywords> all();
}
