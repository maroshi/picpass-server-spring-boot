SELECT kwd1.NAME k1_name, kwd1.id k1_id, kwdpct2.KEYWORDS_ID k2_id, kwd2.NAME k2_name, count(kwdpct2.KEYWORDS_ID) k2_count
FROM KEYWORD kwd1
JOIN PICTURE_KEYWORD_RT kwdpct1 ON kwd1.ID = kwdpct1.KEYWORDS_ID
JOIN PICTURE_KEYWORD_RT kwdpct2 ON kwdpct1.PICTURES_ID = kwdpct2.PICTURES_ID AND kwdpct2.KEYWORDS_ID <> kwd1.id
JOIN KEYWORD kwd2 ON kwdpct2.KEYWORDS_ID = kwd2.ID
WHERE kwd1.NAME = 'q4'
GROUP BY kwd1.NAME, kwdpct2.KEYWORDS_ID, kwd2.name, kwd1.id
;