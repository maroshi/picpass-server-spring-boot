package org.maroshi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.maroshi.dao.Question;
import org.maroshi.dao.QuestionCategory;
import org.maroshi.repository.QuestionCategoryRepository;
import org.maroshi.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/questioncategory")
public class QuestionController {
	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private QuestionCategoryRepository questionCategoryRepository;
	@Autowired
	private QuestionRepository questionRepository;

	public static enum QuestionCategoryEnum {
		PEOPLE("people"), NUMBERS("numbers"), PLACES("places"), PRIVATE("private");

		private String category;

		QuestionCategoryEnum(String category) {
			this.category = category;
		}

		public String getCategory() {
			return category;
		}
	}

	private static String[] initialQuesitonsArr = { "Who is the famous historic actor",
			"Where is the picture with Israeli celebrity", "Where are the African people",
			"Who is the famous scientist", "Who is the last  person,  that texted with you",
			"Who is the last family member, that phone talked with you", "Which picture is related to Israel",
			"Which picture is related to India", "Which picture is related to the metro/underground",
			"Which picture is taken in an aircraft", "Where is a picture from your childhood neighborhood",
			"Where is a picture from the last city, where your car parked",
			"What is the day in month of next Rosh-Hashanah holiday", "How many days was the six days war",
			"What is the month when the almonds bloom", "Your youngest child, what is month of birth",
			"How much USD you spent in your last Ebay purchase", "How many kilometers from home to workplace" };
	private static QuestionCategoryEnum[][] categoryArrArr = {
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PEOPLE, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.PLACES, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, QuestionCategoryEnum.PRIVATE, },
			new QuestionCategoryEnum[] { QuestionCategoryEnum.NUMBERS, QuestionCategoryEnum.PRIVATE, }, };

	@PostConstruct
	void init() {
		long countedQuestionCategories = questionCategoryRepository.count();
		long countedQuestions = questionRepository.count();
		if (countedQuestionCategories > 0 && countedQuestions > 0) {
			return;
		}

		if (countedQuestionCategories == 0) {
			log.info("Empty QuestionCategoryRepository");
			List<QuestionCategory> questionCategoryList = new ArrayList<>();
			questionCategoryList.add(new QuestionCategory(QuestionCategoryEnum.PEOPLE.getCategory()));
			questionCategoryList.add(new QuestionCategory(QuestionCategoryEnum.NUMBERS.getCategory()));
			questionCategoryList.add(new QuestionCategory(QuestionCategoryEnum.PLACES.getCategory()));
			questionCategoryList.add(new QuestionCategory(QuestionCategoryEnum.PRIVATE.getCategory()));
			questionCategoryRepository.saveAll(questionCategoryList);
			log.info("Initialized QuestionCategoryRepository");
		}

		if (countedQuestions == 0) {
			log.info("Empty QuestionRepository");
			List<Question> questionList = new ArrayList<>();
			for (int i = 0; i < initialQuesitonsArr.length; i++) {
				// get the question text
				Question currQuestion = new Question();
				currQuestion.setText(initialQuesitonsArr[i]);

				// get the question categories
				List<QuestionCategory> currQuestionCategoryList = new ArrayList<>();
				QuestionCategoryEnum[] categoryArr = categoryArrArr[i];
				for (QuestionCategoryEnum questionCategoryEnum : categoryArr) {
					Long questionCategoryId = Integer.toUnsignedLong(questionCategoryEnum.ordinal() + 1);
					Optional<QuestionCategory> currQuestionCategoryOption = questionCategoryRepository
							.findById(questionCategoryId);
					QuestionCategory currQuestionCategory = currQuestionCategoryOption.get();
					currQuestionCategoryList.add(currQuestionCategory);
				}
				currQuestion.setQuestionCategories(currQuestionCategoryList);

				questionList.add(currQuestion);
			}

			questionRepository.saveAll(questionList);
			log.info("Initialized QuestionRepository");
		}
	}
}
