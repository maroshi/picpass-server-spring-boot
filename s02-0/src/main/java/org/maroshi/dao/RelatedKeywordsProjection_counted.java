package org.maroshi.dao;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "embedKeywords", types = { RelatedKeywords.class })
public interface RelatedKeywordsProjection_counted {
	Long   getId();
	String getName();
	int    getCount();
}
