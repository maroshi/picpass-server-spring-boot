package org.maroshi.controller;

import java.net.URISyntaxException;

import org.maroshi.dto.PictureDTO;
import org.maroshi.repository.KeywordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

@RequestMapping("/keywords")
@RestController
public class KeywordController {
	@SuppressWarnings("unused")
	@Autowired
	private KeywordRepository keywordRepository;
	@GetMapping("/counts")
	public ResponseEntity<?> listingFn(@Valid @RequestBody PictureDTO pictureDTO, BindingResult result)
			throws URISyntaxException {
		return null;
	}
}
