package org.maroshi.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.maroshi.dao.PictureFile;
import org.maroshi.repository.PictureFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/pictureFiles")
public class PictureFileController {
	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private PictureFileRepository pictureFileRepository;

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<?> handleFileUpload(@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "name", required = true) String fileName) throws URISyntaxException {
		String logMsg = "";
		PictureFile pictureFile = null;
		try {
			byte[] bytes = file.getBytes();
			log.debug("Successfully uploaded " + bytes.length + " bytes, from '" + fileName + "' !");
			pictureFile = new PictureFile(fileName, bytes);
			pictureFileRepository.save(pictureFile);
			logMsg = "Successfully stored '" + fileName + "' : size = " + bytes.length + ", hash = "
					+ pictureFile.getHash() + " !";

			log.debug(logMsg);
		} catch (Exception e) {
			logMsg = "Failed to upload " + fileName + ", inspect log file, message => " + e.getMessage();
			log.error(logMsg);
			return ResponseEntity.badRequest().body(logMsg);
		}
		// Compute the generated URI
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(pictureFile.getId()).toUri();
		location = new URI(location.toString().replace("/upload", ""));

		return ResponseEntity.created(location).build();
	}

	@RequestMapping(value = "/uploadUrl", method = RequestMethod.POST)
	public HttpEntity<?> uploadFromURL(@RequestParam(name="returnImage", defaultValue = "false") Boolean returnImage, 
			@RequestParam("src") String urlPath) throws IOException, URISyntaxException {
		byte[] contents = null;
		String fileName = null;
		PictureFile pictureFile =null;
		URL url  = null;

		InputStream is = null;
		try {
			url = new URL(urlPath);
			
			fileName = url.getPath();
			fileName = fileName.substring(fileName.lastIndexOf('/') + 1);

			is = url.openStream();
			contents = IOUtils.toByteArray(is);
			log.debug("Successfully downloaded image from : " + urlPath);
			
			pictureFile = new PictureFile(fileName, contents);
			pictureFileRepository.save(pictureFile);

			log.debug("Successfully stored '" + fileName + "' : size = " + contents.length + ", hash = "
					+ pictureFile.getHash() + " !");

		} catch (IOException e) {
			String errMsg = String.format("Failed while reading stream from %s: %s", url.toExternalForm(),
					e.getMessage());
			log.error(errMsg, e);
			ResponseEntity<String> response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errMsg);
			return response;
		} finally {
			if (is != null) {
				is.close();
			}
		}
		
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(pictureFile.getId()).toUri();
		location = new URI(location.toString().split("\\?")[0].replace("/uploadUrl", "/download"));

		ResponseEntity<String> response = null;
		if (returnImage == true) {
			response = ResponseEntity.status(HttpStatus.OK).
				body(String.format("<img src=\"%s\" alt=\"%s\"></img>", location, location));
		} else {
			response = ResponseEntity.created(location).build();
		}
		return response;
	}
	
	@RequestMapping(value = "/pic/download/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<byte[]> downloadPicture(@PathVariable Long id) throws IOException {
		Optional<PictureFile> pictureFileOpt = pictureFileRepository.findById(id);
		byte[] contents = null;
		String mimeTypeFromName = null;
		String mimeTypeFromStream = null;
		String mimeType = null;
		ResponseEntity<byte[]> response = null;
		try {
			contents = pictureFileOpt.get().getData();
			mimeTypeFromName = URLConnection.guessContentTypeFromName(pictureFileOpt.get().getName());
			log.debug(String.format("1. mime from name   '%s' : %s\n", pictureFileOpt.get().getName(), mimeTypeFromName));
			mimeTypeFromStream = URLConnection
					.guessContentTypeFromStream(new BufferedInputStream(new ByteArrayInputStream(contents)));
			log.debug(
					String.format("2. mime from stream '%s' : %s\n", pictureFileOpt.get().getName(), mimeTypeFromStream));

			if (mimeTypeFromStream.equals(mimeTypeFromName) == false) {
				log.warn(String.format("Unknown mime-type for PicFile %d '%s'. Computed mime-type from name is '%s', and Computed mime-type from data is '%s'. Taking mime-type from data!", 
						pictureFileOpt.get().getId(),
						pictureFileOpt.get().getName(),
						mimeTypeFromName,
						mimeTypeFromStream));
			}
			// if cannot guess mime-type, assign 'application/octet-stream' mime-type.
			// which will normally cause file download in interactive browsers.
			mimeType = (mimeTypeFromStream == null) ? MediaType.APPLICATION_OCTET_STREAM_VALUE : mimeTypeFromStream;

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType(mimeType));

			response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			response = new ResponseEntity<byte[]>( HttpStatus.NOT_FOUND);			
		}
		catch (Exception e) {
			log.error("Failed download PicFile " + id, e);
			response = new ResponseEntity<byte[]>( HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
}
