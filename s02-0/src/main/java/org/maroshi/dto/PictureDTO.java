package org.maroshi.dto;

import java.util.List;

import org.maroshi.dao.Keyword;

import lombok.Data;

@Data
public class PictureDTO {
	private Long   id;
	private String name;
	private int    width;
	private int    height;
	private List<Keyword> keywords;
	private Long pictureFileId;
	private String pictureFileUrl;
}
