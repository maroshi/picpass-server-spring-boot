package org.maroshi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class S02Application {

	public static void main(String[] args) {
		SpringApplication.run(S02Application.class, args);
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
	    return new WebMvcConfigurer() {
	        @Override
	        public void addCorsMappings(CorsRegistry registry) {
	            registry.addMapping("/api")
	                    .allowedMethods("GET", "POST", "PUT", "DELETE", "PUT","OPTIONS","PATCH", "DELETE")
	                    .allowedHeaders("Authorization", "Cache-Control", "Content-Type")
	                    .allowedOrigins("https://picpass01.homeil.org")
	                    .allowCredentials(false)
	                    .maxAge(-1);
	        }
	    };
	}
	
}
