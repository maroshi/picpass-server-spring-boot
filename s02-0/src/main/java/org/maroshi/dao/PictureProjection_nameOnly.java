package org.maroshi.dao;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "nameOnly", types = { Picture.class })
public interface PictureProjection_nameOnly {
	Long   getId();
	String getName();
}