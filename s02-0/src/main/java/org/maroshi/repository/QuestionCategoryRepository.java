package org.maroshi.repository;

import org.maroshi.dao.QuestionCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface QuestionCategoryRepository extends CrudRepository<QuestionCategory, Long> {

	@Query(nativeQuery = true, value = "SELECT count(*) FROM QUESTION_CATEGORY")
	long count();
}
