package org.maroshi.dao;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "nameOnly", types = { Keyword.class })
public interface KeywordProjection_nameOnly {
	Long   getId();
	String getName();
}
