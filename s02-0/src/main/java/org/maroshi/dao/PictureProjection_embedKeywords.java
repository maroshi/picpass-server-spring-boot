package org.maroshi.dao;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "embedKeywords", types = { Picture.class })
public interface PictureProjection_embedKeywords {
	Long   getId();
	String getName();
	List<Keyword> getKeywords();
}