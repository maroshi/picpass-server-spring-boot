package org.maroshi.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.maroshi.dao.Keyword;
import org.maroshi.dao.Picture;
import org.maroshi.dao.PictureFile;
import org.maroshi.dto.PictureDTO;
import org.maroshi.repository.KeywordRepository;
import org.maroshi.repository.PictureFileRepository;
import org.maroshi.repository.PictureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/pictures")
public class PictureController {
	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private PictureRepository pictureReposity;
	@Autowired
	private KeywordRepository keywordRepository;
	@Autowired
	private PictureFileRepository pictureFileRepository;
	@Autowired
	private PictureFileController pictureFileController;

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<byte[]> downloadPicture(@PathVariable Long id) throws IOException {
		ResponseEntity<byte[]> responseEnt = null;
		
		Picture currPic = pictureReposity.findById(id).get();
		try {
			responseEnt = pictureFileController.downloadPicture(currPic.getPictureFile().getId());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseEnt;
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ResponseEntity<?> insert(@Valid @RequestBody PictureDTO pictureDTO, BindingResult result)
			throws URISyntaxException {
		
		PictureFile pictureFile = null;

		if (pictureDTO.getId() != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}

		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
		}

		if (pictureDTO.getPictureFileId() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result + "\nEmpty/Missing mandatory field, 'pictureFileId' as long number!");
		}
		
		pictureFile = pictureFileRepository.findById(pictureDTO.getPictureFileId()).get();
		if (pictureFile == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result + "\nRecord identified as 'pictureFileId' = "+pictureDTO.getPictureFileId()+" not found!");
		}

		Picture newPicture = persistPicture(pictureDTO, pictureFile);

		// Compute the generated URI
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(newPicture.getId()).toUri();
		location = new URI(location.toString().replace("/insert", ""));

		return ResponseEntity.created(location).build();
	}

	@RequestMapping(value = "/insertUrl", method = RequestMethod.POST)
	public ResponseEntity<?> insertUrl(@Valid @RequestBody PictureDTO pictureDTO, BindingResult result)
			throws URISyntaxException, IOException {
		
		PictureFile pictureFile = null;

		if (pictureDTO.getId() != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}

		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
		}

		if (pictureDTO.getPictureFileUrl() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result + "\nEmpty/Missing mandatory field, 'pictureFileUrl' as text!");
		}
		
		
		byte[] contents = null;
		String fileName = null;
		URL url  = null;

		InputStream is = null;
		try {
			url = new URL(pictureDTO.getPictureFileUrl());
			
			fileName = url.getPath();
			fileName = fileName.substring(fileName.lastIndexOf('/') + 1);

			is = url.openStream();
			contents = IOUtils.toByteArray(is);
			log.debug("Successfully downloaded image from : " + pictureDTO.getPictureFileUrl());
			
			pictureFile = new PictureFile(fileName, contents);
			pictureFileRepository.save(pictureFile);

			log.debug("Successfully stored '" + fileName + "' : size = " + contents.length + ", hash = "
					+ pictureFile.getHash() + " !");

		} catch (IOException e) {
			String errMsg = String.format("Failed while reading stream from %s: %s", url.toExternalForm(),
					e.getMessage());
			log.error(errMsg, e);
			ResponseEntity<String> response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errMsg);
			return response;
		} finally {
			if (is != null) {
				is.close();
			}
		}

		
		Picture newPicture = persistPicture(pictureDTO, pictureFile);

		// Compute the generated URI
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(newPicture.getId()).toUri();
//		location = new URI(location.toString().replace("/insertUrl", ""));

		return ResponseEntity.created(location).build();
	}

	@RequestMapping(value = "/{id}/update1", method = RequestMethod.PUT)
	public ResponseEntity<?> updateKeywords(@PathVariable Long id, @Valid @RequestBody List<Keyword> loadedKeywordsList, BindingResult result)
			throws URISyntaxException, IOException {

		Optional<Picture> retivedPicture = pictureReposity.findById(id);
		Picture modifiedPicture = retivedPicture.get();
		List<Keyword> persistKeywords = new ArrayList<>();

		// persist all relevant keywords
		loadKeywords(loadedKeywordsList, persistKeywords);
		modifiedPicture.setKeywords(persistKeywords);

		pictureReposity.save(modifiedPicture);

		// Compute the generated URI
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.buildAndExpand(modifiedPicture.getId()).toUri();
		location = new URI(location.toString().replace("/update1", "/keywords"));

		return ResponseEntity.created(location).build();
	}

	private Picture persistPicture(PictureDTO pictureDTO, PictureFile pictureFile) {
		Picture persistPicture = new Picture();
		persistPicture.setName(pictureDTO.getName());
		persistPicture.setPictureFile(pictureFile);
		persistPicture.setWidth(pictureDTO.getWidth());
		persistPicture.setHeight(pictureDTO.getHeight());
		List<Keyword> persistKeywords = new ArrayList<>();

		// persist all relevant keywords
		loadKeywords(pictureDTO.getKeywords(), persistKeywords);
		persistPicture.setKeywords(persistKeywords);

		pictureReposity.save(persistPicture);
		return persistPicture;
	}

	private void loadKeywords(List<Keyword> keywordsList, List<Keyword> persistKeywords) {
		for (Keyword keywordDTO : keywordsList) {
			Optional<Keyword> persistKeyword = keywordRepository.findByName(keywordDTO.getName());
			if (persistKeyword.isPresent()) {
				persistKeywords.add(persistKeyword.get());
				continue;
			}
			Keyword tempKeyword = new Keyword(keywordDTO.getName(), keywordDTO.getCategory());
			persistKeywords.add(keywordRepository.save(tempKeyword));
		}
	}
	
}
