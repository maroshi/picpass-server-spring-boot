// Unique ID for the className.
var MOUSE_VISITED_CLASSNAME = 'crx_mouse_visited';
var _webServiceUrl = '/api/';

// Previous dom, that we want to track, so we can remove the previous styling.
var prevDOM = null;

var activeLink = null;
var activeImg = null;
var capturedImg = null;

// Add radiobuttons hardcoded styles
const css = document.createElement('style');


// Add popup dialog box inside keywords list place holder

var maxRowsCount = 8;
var availableKeywordsArr = [];
var allKeywordsObjArr = [];
var submitedKeywordsObjArr = [];

function terminateDialog() {
    $("#dialog-3").dialog("close");
//    let appDialog = document.getElementById('dialog-3');
//    appDialog.show();
    // appDialog.showModal();
}
function sucessfullPost(msg) {
    // console.log("success : ");
    // console.dir(msg);

    $.getJSON(`${_webServiceUrl}keywordPicsCounts/search/all`,
        function (respData) {
            let keywordCountedObjsArr = respData._embedded.keywordPicsCounts;
        });

    terminateDialog();
}

$(function dialogWraper() {
    $("#dialog-3").dialog({
        autoOpen: false,
        width: 920,
        buttons: {
            "Submit keywords": function () {
                // Collect keywords
                let rowsArr = $(".imagemarker-dialog-section");
                let submitObj = {
                    "keywords": [],
                }
                rowsArr.each(function (i) {
                    let row = rowsArr[i];
                    let keywordText = row.getElementsByTagName('INPUT')[0].value;
                    // console.log('text:' + keywordText);

                    // continue to next line on empty text
                    if (keywordText.length !== 0) {

                        // Read the selected label suffix (one letter)
                        let selectedCategoryLabel = row.getElementsByClassName('ui-checkboxradio-checked');
                        (selectedCategoryLabel.length == 0) ?
                            selectedCategoryLabel = '0' :
                            selectedCategoryLabel = selectedCategoryLabel[0]["htmlFor"].slice(-1);

                        // Calculate category from label suffix
                        let selectedCategory = 'undefined';
                        switch (selectedCategoryLabel) {
                            case '1':
                                selectedCategory = 'noun';
                                break;

                            case '2':
                                selectedCategory = 'descr';
                                break;

                            case '3':
                                selectedCategory = 'verb';
                                break;
                        }
                        // console.log('selected:'+selectedCategory);
                        let keywordObj = {
                            "name": keywordText,
                            "category": selectedCategory,
                            "picsCount": 0
                        }
                        submitObj.keywords.push(keywordObj);
                    }
                    submitedKeywordsObjArr = submitObj.keywords;
                });
                // console.dir(submitObj);

                let pictureId = $('#img_title').text();

                $.ajax({
                    type: "PUT",
                    url: `${_webServiceUrl}pictures/${pictureId}/update1`,
                    data: JSON.stringify(submitObj.keywords),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).then(function (msg) { sucessfullPost(msg); }, function (errMsg) {
                    if (errMsg.status === 201) {
                        // successful post
                        sucessfullPost(errMsg);
                    } else {
                        // failed post
                        console.log("failure : " + errMsg.responseText);
                        alert("failure : " + errMsg.responseText);
                    }
                });
            },
            Cancel: function () {
                terminateDialog();
            },
            'Delete Pic': function () {
                let pictureId = _currentFigureElm.getElementsByTagName('details')[0].textContent;

                $.ajax({
                    type: "DELETE",
                    url: `${_webServiceUrl}pictures/${pictureId}`,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).then(function (msg) { sucessfullPost(msg); }, function (errMsg) {
                    if (errMsg.status === 201) {
                        // successful post
                        // sucessfullPost(errMsg);
                    } else {
                        // failed post
                        console.log("failure : " + errMsg.responseText);
                        alert("failure : " + errMsg.responseText);
                    }
                });
                terminateDialog();
            },
            'Clear keywords': function () {
                $('.keyword-textbox').val('');
                $('.pics-count').each((idx, divElm) => { divElm.innerText = '' });
                $('input[type=radio]').each((idx, radioElm) => { radioElm.checked = false; });
                $('input[type=radio]').button('refresh');
            }
        }

    });
});

function updateRelatedKeywords(keywordObj) {
    // if keywordObj has relatedKeywords property need not update
    if (keywordObj.hasOwnProperty('relatedKeywords')) {
        return;
    }
    // need to retireve the relatedKeywords property from server

    $.getJSON(`${_webServiceUrl}relatedKeywordses/search/findRelatedByName?name=${keywordObj.name}`,
        function (respData) {
            let relatedKeywordsObjArr = respData._embedded.relatedKeywordses;
            // console.dir(keywordCountedObjsArr);
            keywordObj.relatedKeywords = relatedKeywordsObjArr;
        });


}

function loadRelatedKeywordsText(textInputElm, keywordObj) {
    // find the corect Div elment
    let relatedKeywordsDivElm = textInputElm.parentElement.parentElement.getElementsByClassName('related-keywords')[0];
    let relatedKeywordsStr = '';
    // read all the related keywords into string and assign to div elem
    keywordObj.relatedKeywords.forEach(function (item) {
        if (item.count > 1) {
            let a = document.createElement('a');
            let linkText = document.createTextNode(` ${item.name}(${item.count})`);
            a.appendChild(linkText);
            a.setAttribute('target', '_blank');
            a.href = `https://localhost:8443/k1.html?k=${item.name}`;
            relatedKeywordsDivElm.appendChild(a);
        }
    });

}

function pupulateKeywordRow(textInputElm, keywordObj) {

    textInputElm.value = keywordObj.name;

    // identify category radio button
    let radioButtonIdx = 0;
    switch (keywordObj.category) {
        case "noun":
            radioButtonIdx = 1;
            break;

        case "descr":
            radioButtonIdx = 2;
            break;

        case "verb":
            radioButtonIdx = 3;
    }
    let row = textInputElm.id.slice(-3, -2);
    let radioButtonId = `#radio${row}-${radioButtonIdx}`;
    $(radioButtonId).click();
    let keywordCountDivId = `#keyword-count-${row}`;
    $(keywordCountDivId).html(`<a href="https://localhost:8443/k1.html?k=${textInputElm.value}" target="_blank">${keywordObj.picsCount}</a>`);
    // populated related keyword if there are any
    //_availableKeywordsArr
    let globleKeywordObj = _availableKeywordsArr.find(o => o.name === keywordObj.name);
    $(keywordCountDivId).html(`<a href="https://localhost:8443/k1.html?k=${textInputElm.value}" target="_blank">${globleKeywordObj.picsCount}</a>`);
    if (!keywordObj.hasOwnProperty('relatedKeywords')) {
        $.getJSON(`${_webServiceUrl}relatedKeywordses/search/findRelatedByName?name=${keywordObj.name}`)
            .done(function (resp) {
                let relatedKeywordsObjArr = resp._embedded.relatedKeywordses;
                keywordObj.relatedKeywords = relatedKeywordsObjArr;
                loadRelatedKeywordsText(textInputElm, keywordObj);
            });
        return;
    };
    if (keywordObj.relatedKeywords.length == 0) return;
    loadRelatedKeywordsText(textInputElm, keywordObj);
}

// Add listner on current hovered figure
var _currentFigureElm;
$(document).mouseover(function (e) {
    let currentElmId = $(e.target).attr('id');
    if (typeof currentElmId === "undefined") {
        return;
    }
    let currPrntElm = $(`#${currentElmId}`)[0].parentElement;
    if (currPrntElm.tagName !== "FIGURE") {
        return;
    }
    _currentFigureElm = currPrntElm;
    // console.dir(_currentFigureElm); // i just retrieved the id for a demo
});


function onPictureUpdate (e) {
    // console.log("keypressed with keycode: " + e.keyCode);
    /* trigger is ` charachter*/
	let urlSegmentsArr = this.src.split('/');
    let picId = urlSegmentsArr[urlSegmentsArr.length - 1];
    let picImgSrc = this.src;

    // init submitedKeywordsObjArrread from current picture
    submitedKeywordsObjArr = [];
    $.getJSON(`/api/pictures/${picId}/keywords`, function(respData) {
    	submitedKeywordsObjArr = respData._embedded.keywords;
    	
    	$('#img_src').attr("src", picImgSrc);
    	$('#img_href').attr("href", picImgSrc);
    	$('#img_title').text(picId);

        // Clear previous rows, except first
        $('.imagemarker-dialog-section:first-of-type input').val("");
        while ($('.imagemarker-dialog-section').length > 1) {
            $('.imagemarker-dialog-section').last().remove();
        }
        $('.pics-count,.related-keywords').html('');



        // Rebuild all rows 

        for (let rowsCount = 2; rowsCount <= maxRowsCount; rowsCount++) {
            let newRow = $('#imagemarker-row1').clone();
            newRow.attr("id","imagemarker-row" + rowsCount);
            newRow.html(newRow.html().replace(/radio1/g, "radio" + rowsCount));
            newRow.html(newRow.html().replace(/text1/g, "text" + rowsCount));
            newRow.html(newRow.html().replace(/radioset1/g, "radioset" + rowsCount));
            newRow.html(newRow.html().replace(/keyword-count-1/g, "keyword-count-" + rowsCount));
            $('#form1').append(newRow);
        }
        // Insert current keywords info
        $('.imagemarker-dialog-section').each(function (row, element) {
            if (submitedKeywordsObjArr.length > 0 && row < submitedKeywordsObjArr.length) {
                let textInput = element.getElementsByTagName('input')[0];
                pupulateKeywordRow(textInput, submitedKeywordsObjArr[row]);
            }
        });
        // Attach autocomplete functionality to each textbox
        $(".keyword-textbox").autocomplete({
            source: availableKeywordsArr,
            select: function (event, ui) {
                let keywordIdx = availableKeywordsArr.indexOf(ui.item.value);
                let keywordObj = allKeywordsObjArr[keywordIdx];
                let textInputElm = $(`#${event.target.id}`)[0];
                pupulateKeywordRow(textInputElm, keywordObj);
            }
        });
        // Update keyword rows when text box lose keybourad focus
        $(".keyword-textbox").blur((event) => {
            let currTargetElm = event.currentTarget;
            if (currTargetElm.value.length > 0) {
                // console.dir(event.currentTarget);
                // console.log(`current keyword is: ${currTargetElm.value}`);
                let keywordIdx = availableKeywordsArr.indexOf(currTargetElm.value);
                if (keywordIdx > -1) {
                    let keywordObj = allKeywordsObjArr[keywordIdx];
                    pupulateKeywordRow(currTargetElm, keywordObj);
                }
            }
        });

        // Finally open the dialog box and activate it for UI
        $(".radioset").buttonset();
        // workarounds for jquery-ui bugs
        $('#form1 .radioset label').each(function (idx, labelElm){
        	while (labelElm.children.length > 2) {
        		labelElm.children[2].remove();
        	}
        });
        if ($('.imagemarker-dialog-form').length > 1) $('.imagemarker-dialog-form')[1].remove();
        
        $("#dialog-3").dialog("open");
        $("#dialog-3").dialog("moveToTop");
    });
}