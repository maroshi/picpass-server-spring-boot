package org.maroshi.dao;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "question")
public class Question {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   id;

	private String text;
	
	@ManyToMany
	@JoinTable(name="QUESTION_CATEGORY__QUESTION_RT")
	private List<QuestionCategory> questionCategories;	

	public Question() {
		this.text = "undefined";
	}

	public Question(String name) {
		this.text = name;
	}
}
