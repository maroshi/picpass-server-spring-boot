package org.maroshi.dao;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Picture {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   id;
	@NotNull
	private String name;
	private int    width;
	private int    height;
	@Transient
	private Long   id_;
	@ManyToMany
	@JoinTable(name="PICTURE_KEYWORD_RT")
	private List<Keyword> keywords;
    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    private PictureFile pictureFile;
    
    @PostLoad public void loadId_() { id_ = id;};

}
