package org.maroshi.repository;

import org.maroshi.dao.Picture;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface PictureRepository extends CrudRepository<Picture, Long> {


}
