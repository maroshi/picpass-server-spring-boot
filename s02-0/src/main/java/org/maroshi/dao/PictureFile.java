package org.maroshi.dao;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;

import org.springframework.util.DigestUtils;

import lombok.Data;

/**
 * @return
 */
@Data
@Entity
public class PictureFile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	@Lob
	@NotNull
	private byte[] data;
	private int size;
	@NotNull
	@Column(unique = true)
	private String hash;
	@OneToOne(fetch = FetchType.EAGER,
			mappedBy = "pictureFile")
//	@JoinColumn(name = "picture_id", nullable = false)
	private Picture picture;

	public PictureFile(String name, byte[] data) {
		this.name = name;
		this.data = data;
		size = data.length;
		hash = DigestUtils.md5DigestAsHex(data);
	}

	public PictureFile() {
		this.name = "undefined";
		this.data = null;
		size = 0;
		hash = "not intilized";
	}
}
