package org.maroshi.dao;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "question_category")
public class QuestionCategory {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String category;
	
	@ManyToMany(mappedBy="questionCategories", cascade = CascadeType.ALL)
	private List<Question> questions;

	public QuestionCategory() {
		this.category = "undefined";
	}
	
	public QuestionCategory(String name) {
		this.category = name;
	}
}
