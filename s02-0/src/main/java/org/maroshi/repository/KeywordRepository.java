package org.maroshi.repository;

import java.util.List;
import java.util.Optional;

import org.maroshi.dao.Keyword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface KeywordRepository extends CrudRepository<Keyword, Long> {
	Optional<Keyword> findByName(@Param("name") String name);
	List<Keyword> findByCategory(@Param("category") String category);
}
