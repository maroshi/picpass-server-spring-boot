package org.maroshi.repository;

import org.maroshi.dao.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(methods = RequestMethod.GET, allowCredentials = "false")
@RepositoryRestResource
public interface QuestionRepository extends CrudRepository<Question, Long> {
	@Query(nativeQuery = true, value = "SELECT count(*) FROM QUESTION")
	long count();
}
