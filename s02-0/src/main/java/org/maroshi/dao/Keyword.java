package org.maroshi.dao;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "keyword",
       indexes = {@Index(name = "idx_keyword_name",  columnList="name", unique = false)})
public class Keyword {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   id;
	@NotNull 
	private String name;
	private String category;
	@ManyToMany(mappedBy="keywords", cascade = CascadeType.ALL)
	private List<Picture> pictures;

	public Keyword() {
		this.name = "undefined";
		this.category = "undefined";
	}

	public Keyword(String name, String category) {
		this.name = name;
		this.category = category;
	}
}
