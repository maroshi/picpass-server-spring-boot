package org.maroshi.dao;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "embedPictures", types = { Keyword.class })
public interface KeywordProjection_embedPictures {
	Long   getId();
	String getName();
	List<Picture> getPictures();
}