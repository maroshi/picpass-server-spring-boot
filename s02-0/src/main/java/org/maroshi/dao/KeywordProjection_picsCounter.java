package org.maroshi.dao;

public interface KeywordProjection_picsCounter {
	Long getId();
	Long getPicsCount();
}
